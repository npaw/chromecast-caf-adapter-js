/* global cast */

var youbora = require('youboralib')
var manifest = require('../../manifest.json')

var AdsAdapter = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  getPlayerName: function () {
    return 'Chromecast CAF Ads'
  },

  getPlayhead: function () {
    return this.player.getBreakClipCurrentTimeSec()
  },

  getDuration: function () {
    return this.player.getBreakClipDurationSec()
  },

  getPosition: function () {
    var adapter = this.plugin ? this.plugin.getAdapter() : null
    var ret = youbora.Constants.AdPosition.Midroll
    if (adapter) {
      if (!adapter.flags.isJoined || adapter.getPlayhead() < 1) {
        ret = youbora.Constants.AdPosition.Preroll
      } else if (adapter.getPlayhead() + 1 >= adapter.getDuration()) {
        ret = youbora.Constants.AdPosition.Postroll
      }
    }
    return ret
  },

  getTitle: function () {
    return this.player.getMediaInformation().metadata.title;
  },

  getResource: function () {
    return this.player.getMediaInformation().contentUrl || this.player.getMediaInformation().contentId;
  },

  getCreativeId: function () {
    return this.clipId
  },

  getIsVisible: function () {
    return true
  },

  getIsFullscreen: function () {
    return true
  },

  getIsSkippable: function () {
    return this.isSkippable
  },

  getGivenAds: function () {
    return this.adsInBreak
  },

  getBreaksTime: function () {
    var breaks = this.player.getBreaks()
    var cuePoints = []
    for (var breakN in breaks) {
      var position = breaks[breakN].position
      var adapter = this.plugin ? this.plugin.getAdapter() : null
      if (position === -1 && adapter) {
        position = adapter.getDuration()
      }
      if (cuePoints.indexOf(position) === -1) {
        cuePoints.push(position)
      }
    }
    return cuePoints
  },

  getGivenBreaks: function () {
    var breaks = this.player.getBreaks()
    var cuePoints = []
    for (var breakN in breaks) {
      var position = breaks[breakN].position
      var adapter = this.plugin ? this.plugin.getAdapter() : null
      if (position === -1 && adapter) {
        position = adapter.getDuration()
      }
      if (cuePoints.indexOf(position) === -1) {
        cuePoints.push(position)
      }
    }
    return cuePoints.length
  },

  registerListeners: function () {
    var Events = cast.framework.events

    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, false, 1200)

    // Register listeners
    this.references = {}
    this.references[Events.EventType.BREAK_ENDED] = this.endListener.bind(this)
    this.references[Events.EventType.BREAK_CLIP_LOADING] = this.playListener.bind(this)
    this.references[Events.EventType.BREAK_CLIP_STARTED] = this.joinListener.bind(this)
    this.references[Events.EventType.BREAK_CLIP_ENDED] = this.stopListener.bind(this)
    this.references[Events.EventType.BREAK_STARTED] = this.breakStartListener.bind(this)
    this.references[Events.EventType.BREAK_ENDED] = this.breakEndListener.bind(this)
    this.references[Events.EventType.PAUSE] = this.pauseListener.bind(this)
    this.references[Events.EventType.PLAYING] = this.playingListener.bind(this)

    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key])
    }
  },

  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      this.references = {}
    }
  },

  playListener: function (e) {
    var adapter = this.plugin ? this.plugin.getAdapter() : null
    if (adapter) {
      adapter.fireStart()
      adapter.firePause()
    }
    if (typeof e !== 'undefined') {
      this.isSkippable = e.whenSkippable !== undefined
      this.clipId = e.breakClipId
    } else {
      this.isSkippable = false
      this.clipId = null
    }
  },

  joinListener: function (e) {
    this._quartileTimer = new youbora.Timer(this.sendQuartile.bind(this), 1000)
    this._quartileTimer.start()
    var adapter = this.plugin ? this.plugin.getAdapter() : null
    if (adapter) adapter.firePause()
    if (this.plugin && this.plugin.isBreakStarted) {
      this.fireStart()
      this.fireJoin()
    }
  },

  breakStartListener: function(e) {
    this.adsInBreak = e.total
    this.fireBreakStart()
    this.fireStart()
    this.fireJoin()
  },

  breakEndListener: function(e) {
    this.adsInBreak = undefined
  },

  stopListener: function (e) {
    this.fireStop()
  },

  endListener: function (e) {
    this.fireBreakStop()
    var adapter = this.plugin ? this.plugin.getAdapter() : null
    if (adapter) adapter.fireResume()
  },

  sendQuartile: function (e) {
    var playhead = this.getPlayhead()
    var duration = this.getDuration()
    if (playhead > duration / 4) {
      this.fireQuartile(1)
      if (playhead > duration / 2) {
        this.fireQuartile(2)
        if (playhead > duration * 0.75) {
          this.fireQuartile(3)
          if (this._quartileTimer) this._quartileTimer.stop()
        }
      }
    }
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    if (!this.flags.isBuffering) {
      this.firePause()
    }
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    this.fireResume()
  }
})

module.exports = AdsAdapter
