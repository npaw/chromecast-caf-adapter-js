/* global cast */
var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.ChromecastCAF = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.player.getCurrentTimeSec()
  },

  /** Override to return current playrate */
  getPlayrate: function () {
    var ret = this.player.getPlaybackRate()
    if (!ret && !this.flags.isPaused) {
      // If is not paused but the playrate is 0, is wrong and can break the playhead monitor,
      // so we assume its 1
      ret = 1
    }
    return ret
  },

  /** Override to return dropped frames since start */
  getDroppedFrames: function () {
    return this.castMediaElement? this.castMediaElement.webkitDroppedFrameCount : null
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.player.getDurationSec()
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    return this.bitrate || 0
  },

  /** Override to return user bandwidth throughput */
  getThroughput: function () {
    var tp = this.throughput || -1
    if (!this.throughput) {
      if (this.castMediaElement && this.castMediaElement.webkitVideoDecodedByteCount && this.castMediaElement.webkitAudioDecodedByteCount) {
        var video = this.castMediaElement.webkitVideoDecodedByteCount
        var audio = this.castMediaElement.webkitAudioDecodedByteCount
        var prevCount = this.actualCount || 0
        if (video && audio > video) {
          this.actualCount = video
        } else {
          this.actualCount = audio + video
        }
        tp = Math.round((this.actualCount - prevCount) / (this.plugin._ping.interval / 1000))
      }
    }
    return tp
  },

  /** Override to return rendition */
  getRendition: function () {
    var width = this.castMediaElement? this.castMediaElement.videoWidth : null
    var height = this.castMediaElement? this.castMediaElement.videoHeight : null
    return youbora.Util.buildRenditionString(width, height, this.getBitrate())
  },

  /** Override to return title */
  getTitle: function () {
    return this.player.getMediaInformation().metadata.title
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return this.getDuration() === -1
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.player.getMediaInformation().contentUrl || this.player.getMediaInformation().contentId
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return cast && cast.framework ? cast.framework.VERSION : null
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Chromecast'
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    this.castMediaElement = undefined
    for (var object in this.player) {
      if (typeof this.player[object] === 'object') {
        for (var object2 in this.player[object]) {
          if (this.player[object][object2] && this.player[object][object2].id === 'castMediaElement') {
            this.castMediaElement = this.player[object][object2]
          }
        }
      }
    }

    var Events = cast.framework.events
    var eCode = cast.framework.events.DetailedErrorCode

    // DetailedErrorCodes of the errors we dont want to report
    // https://developers.google.com/cast/docs/reference/caf_receiver/cast.framework.events#.DetailedErrorCode
    this.errorFilter = [
      eCode.MEDIAKEYS_UNKNOWN,
      // eCode.MEDIAKEYS_NETWORK,
      eCode.MEDIAKEYS_UNSUPPORTED,
      eCode.MEDIAKEYS_WEBCRYPTO,
      eCode.SEGMENT_NETWORK,
      eCode.HLS_NETWORK_INVALID_SEGMENT,
      eCode.DASH_MANIFEST_NO_MIMETYPE,
      eCode.DASH_INVALID_SEGMENT_INFO,
      eCode.TEXT_UNKNOWN,
      eCode.SEGMENT_UNKNOWN,
      eCode.BREAK_SEEK_INTERCEPTOR_ERROR,
      eCode.APP,
      eCode.IMAGE_ERROR,
      eCode.LOAD_INTERRUPTED,
      eCode.MEDIA_ERROR_MESSAGE
    ]

    /* this.errorMessages = {
      100: 'The media element encountered an unknown error fired from platform.',
      101: 'The media element fired MediaError.MEDIA_ERR_ABORTED error.',
      102: 'The media element fired MediaError.MEDIA_ERR_DECODE error.',
      103: 'The media element fired MediaError.MEDIA_ERR_NETWORK error',
      104: 'The media element fired MediaError.MEDIA_ERR_SRC_NOT_SUPPORTED error.',
      110: 'Cast is unable to add a source buffer to the existing Media source.',
      311: 'Failed to retrieve the master playlist m3u8 file with three retries.',
      312: 'Failed to retrieve the media (bitrated) playlist m3u8 file with three retries.',
      313: 'The request for decryption key did not return a response.',
      314: 'The XhrIO used to request HLS decryption key failed.',
      321: 'The XHR request to get the DASH Manifest failed with no response.',
      322: 'We cannot extract initialization data from the first DASH init segment.',
      331: 'The XHR request to get the DASH Manifest failed with no response.',
      332: 'The segment downloaded for processing contains no media data.',
      411: 'Parsing of the HLS manifest file failed. Or something MPL does not understand yet in the m3u8',
      412: 'Parsing of the media playlist file failed. Or something MPL does not understand yet in the m3u8',
      421: 'When normalizing the Dash manifest, we found not periods in it. This is abnormal.',
      431: 'The smooth manifest does not conform to standard.',
      400: 'The request could not be understood by the server due to malformed syntax. The client SHOULD NOT repeat the request without modifications.',
      401: 'The request requires user authentication',
      404: 'Client can communicate with server but server could not find what was requested',
      408: 'The client did not produce a request within the time that the server was prepared to wait. The client MAY repeat the request without modifications at any later time.',
      502: 'The server, while acting as a gateway or proxy, received an invalid response from the upstream server it accessed in attempting to fulfill the request.',
      503: 'The server is currently unable to handle the request due to a temporary overloading or maintenance of the server. The implication is that this is a temporary condition which will be alleviated after some delay.',
      504: 'The server, while acting as a gateway or proxy, did not receive a timely response from the upstream server specified by the URI.'
    } */

    // Console all events if logLevel=DEBUG
    youbora.Util.logAllEvents(this.player, [null,
      Events.EventType.PLAY,
      Events.EventType.PAUSE,
      Events.EventType.PLAYING,
      Events.EventType.STALLED,
      Events.EventType.SEEKING,
      Events.EventType.SEEKED,
      Events.EventType.ERROR,
      Events.EventType.CLIP_ENDED,
      Events.EventType.BITRATE_CHANGED,
      Events.EventType.SEGMENT_DOWNLOADED,
      Events.EventType.BUFFERING,
      Events.EventType.EMSG,
      Events.EventType.ID3,
      Events.EventType.MEDIA_FINISHED,
      Events.EventType.MEDIA_STATUS,
      Events.EventType.CACHE_HIT,
      Events.EventType.BREAK_CLIP_STARTED,
      Events.EventType.BREAK_ENDED,
      Events.EventType.BREAK_CLIP_LOADING,
      Events.EventType.BREAK_CLIP_STARTED,
      Events.EventType.BREAK_CLIP_ENDED,
      Events.EventType.BREAK_STARTED
    ])

    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, true)

    // Register listeners
    this.references = {}
    this.references[Events.EventType.PLAY] = this.playListener.bind(this)
    this.references[Events.EventType.STALLED] = this.playListener.bind(this)
    this.references[Events.EventType.BUFFERING] = this.bufferingListener.bind(this)
    this.references[Events.EventType.PAUSE] = this.pauseListener.bind(this)
    this.references[Events.EventType.BREAK_CLIP_STARTED] = this.pauseListener.bind(this)
    this.references[Events.EventType.BREAK_ENDED] = this.resumeListener.bind(this)
    this.references[Events.EventType.PLAYING] = this.playingListener.bind(this)
    this.references[Events.EventType.SEEKING] = this.seekingListener.bind(this)
    this.references[Events.EventType.MEDIA_FINISHED] = this.endedListener.bind(this)
    this.references[Events.EventType.ERROR] = this.errorListener.bind(this)
    this.references[Events.EventType.BITRATE_CHANGED] = this.bitrateChangedListener.bind(this)
    this.references[Events.EventType.SEGMENT_DOWNLOADED] = this.segmentListener.bind(this)
    this.references[Events.EventType.BREAK_STARTED] = this.playAdListener.bind(this)
    this.references[Events.EventType.TIME_UPDATE] = this.timeUpdateListener.bind(this)
    this.references[Events.EventType.MEDIA_STATUS] = this.statusListener.bind(this)

    this.hostReferences = {
      onAutoPause: this.autoPauseListener.bind(this)
    }

    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key])
    }
    if (cast && cast.player && cast.player.api &&
      cast.player.api.Player && cast.player.api.Player.getHost && cast.player.api.Player.getHost()) {
      for (var key2 in this.hostReferences) {
        cast.player.api.Player.getHost().addEventListener(key2, this.hostReferences[key2])
      }
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      this.references = {}
      if (cast && cast.player && cast.player.api && cast.player.api.Player &&
        cast.player.api.Player.getHost && cast.player.api.Player.getHost() && this.hostReferences) {
        for (var key2 in this.hostReferences) {
          this.player.getHost().removeEventListener(key2, this.hostReferences[key2])
        }
        this.hostReferences = {}
      }
    }
  },

  statusListener: function (e) {
    if (!this.plugin.getAdsAdapter()) {
      this.plugin.setAdsAdapter(new youbora.adapters.ChromecastCAF.AdsAdapter(this.player))
    }
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    if (!this.flags.isStarted) {
      this.lastPlayhead = null
      this.fireStart()
    }
    if (!this.plugin.getAdsAdapter()) {
      this.plugin.setAdsAdapter(new youbora.adapters.ChromecastCAF.AdsAdapter(this.player))
    }
  },

  playAdListener: function (e) {
    this.playListener()
    this.firePause()
    if (!this.plugin.getAdsAdapter()) {
      var adapter = new youbora.adapters.ChromecastCAF.AdsAdapter(this.player)
      this.plugin.setAdsAdapter(adapter)
      adapter.breakStartListener(e)
    }
  },

  bufferingListener: function (e) {
    if (this.flags.isPaused) {
      if (!this.plugin || !this.plugin.isBreakStarted) {
        this.fireResume()
        this.fireBufferBegin()
      }
    } else {
      this.fireBufferBegin()
    }
    if (!this.flags.isStarted) {
      this.lastPlayhead = null
      this.fireStart()
    }
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    if (!this.flags.isBuffering) {
      this.firePause()
    }
  },

  resumeListener: function (e) {
    this.fireResume()
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    if (!this.plugin || !this.plugin.isBreakStarted) {
      this.fireResume()
      this.fireBufferEnd()
      this.fireJoin()
    }
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    var error = e.error
    var code = e.detailedErrorCode
    if (code) {
      if (this.errorFilter.indexOf(code) >= 0) {
        return // if the code is in the filter list, skip it
      }
      var eCode = cast.framework.events.DetailedErrorCode
      var message = Object.keys(eCode).find(function (key) {
        return eCode[key] === code
      }) || code
      // var message = this.errorMessages[code] || code

      // Check if it's a shaka error
      if (error && error.shakaErrorCode) {
        message = 'Shaka: ' + error.shakaErrorCode + ' , ' + message
      }
      // ---------
      // TODO check for more player errors
  
      if (error === 'LOAD_CANCELLED') {
        this.fireFatalError(code, message)
        this._reset()
      } else {
        this.fireError(code, message)
      }
    }
  },

  /** Listener for 'seeking' event. */
  seekingListener: function (e) {
    this.fireSeekBegin()
  },

  /** Listener for 'ended' event. */
  endedListener: function (e) {
    this.fireStop()
    this._reset()
  },

  bitrateChangedListener: function (e) {
    this.bitrate = e.totalBitrate
  },

  segmentListener: function (e) {
    this.throughput = e.size / (e.downloadTime / 1000) // bytes/s
  },

  timeUpdateListener: function (e) {
    if (!this.flags.isJoined) {
      if (this.lastPlayhead === null || this.lastPlayhead === undefined) {
        this.lastPlayhead = this.getPlayhead()
      } else if (this.lastPlayhead < this.getPlayhead() && this.plugin &&
        (!this.plugin.getAdsAdapter() ||
          (this.plugin.getAdsAdapter() && !this.plugin.getAdsAdapter().flags.isStarted))
      ) {
        this.fireJoin()
      }
    }
  },

  autoPauseListener: function (e) {
    if (e) {
      this.fireResume()
      this.fireBufferBegin()
    } else {
      this.fireBufferEnd()
    }
  },

  _reset: function(e) {
    this.lastPlayhead = null
    this.actualCount = 0
    this.bitrate = null
  }
},
{
  AdsAdapter: require('./ads/adsAdapter')
}
)

module.exports = youbora.adapters.ChromecastCAF
